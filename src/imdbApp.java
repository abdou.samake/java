public class imdbApp {

    public static void main(String[] args) {

        String nomActeur = "Tom Cruz";
        int anneeDeNaissance = 1986;
        String[] titresDeFilm = {
                 "Le dernier Samourai", "Rapport minoritaire", "Top Gun",
                "L'homme de la pluie", "Cocktail", "La momie", "American Traffic"};
        float[] notes = {
                7.7F, 7.6F, 6.9F,
                8.0F, 5.9F, 5.4F, 7.1F
        };

        System.out.println("Nom de l'acteur: " + nomActeur);
        System.out.println("Année de naissance: " + anneeDeNaissance);
        int age = 2023 - anneeDeNaissance;
        System.out.println("age: " + age);
        for (int i = 0; i < titresDeFilm.length; i++) {
            System.out.println(titresDeFilm[i] + " - " + evaluationsFilms(notes[i]));
        }
    }
    static String evaluationsFilms(float note) {
            if(note <= 5.0){
                return "mauvais";
            }
            else if(note > 5.0 && note <= 6.5) {
                return "pas mal";
            }
            else if(note >6.5 && note <= 7.0) {
                return  " bon";
            }
            else if (note > 7.0 && note <= 8.0) {
                return  "tres bon";
            }
            else if(note > 8.0){
                return "incroyable";
            }
            return null;
    }

    }
