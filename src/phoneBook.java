import model.Contact;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class phoneBook {

    public static final String PHONE_BOOK_FILE_PATH = "C:\\Users\\abdou\\cours-java-springBoot\\Cour_JirAWS\\src\\monDocumentTexte.txt";
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String userLastName = getUserInput("Entrer un nom de Famille: ");
        String userFirstName = getUserInput("Entrer un prenom: ");
        String userPhoneNumber = getUserInput("Entrez un numéro de telephone: ");

        Contact newContact = new Contact(userLastName, userFirstName, userPhoneNumber);

        System.out.println(newContact.toString());

        File phoneBooKFile = getOrCreatePhoneBookFile(PHONE_BOOK_FILE_PATH);
        appendContactToPhoneBook(phoneBooKFile, newContact);

        sc.close();
    }
    public static String getUserInput(String userRequest) {
        System.out.println(userRequest);
        return sc.nextLine();
    }
    public static File getOrCreatePhoneBookFile(String phoneBookFilePath) {

        File phoneBooKFile = new File(phoneBookFilePath);

        if(phoneBooKFile.exists()) {
            return phoneBooKFile;
        }
        try {
            phoneBooKFile.createNewFile();
            System.out.println("le fichier a été créé (" + phoneBookFilePath + ")");
            return phoneBooKFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void appendContactToPhoneBook(File phoneBookFile, Contact newContact) {
        try(BufferedWriter filewriter = new BufferedWriter(new FileWriter(phoneBookFile, true))) {
            filewriter.append(newContact.toString());
            // filewriter.append("\n");
            filewriter.append(System.lineSeparator());
            System.out.println("Contact ajouté");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
