public class BouclesApp {

    public static void main(String[] args) {

        int i = 1;
        while(i <= 5) {
            System.out.println("Je ne parle pas français");
            i++;
        }
        System.out.println("valeur de i: " + i);

        int j = 1;
        do{
            System.out.println("Je ne parle pas français");
            j++;
        } while(j <= 0);
        System.out.println();

        for(int k = 1; k <= 5; k++){
            for(int l = 1; l <= 2; l++) {
                System.out.println("k=" + k + " l=" + l + ".Je ne parle pas français");
            }
        }
        System.out.println();
    }
}
