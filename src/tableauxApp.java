public class tableauxApp {

    public static void main(String[] args) {

        int[] numerosLoterie = new int[5];
        numerosLoterie[0] = 7;
        numerosLoterie[1] = 23;
        numerosLoterie[2] = 67;
        numerosLoterie[3] = 37;
        numerosLoterie[4] = 11;

        for(int numeroLoterie: numerosLoterie){
            System.out.println(numeroLoterie);
        }


        int[][] numerosLoterieHebdomadaire = {
                {1, 2, 3, 4, 5},
                {10, 20, 30, 40, 50},
                {13, 24, 36, 47, 58},
                {19, 258, 37, 46, 51}
        };

        for (int i = 0; i < numerosLoterieHebdomadaire.length; i++) {
            for (int j = 0; j < numerosLoterieHebdomadaire[i].length; j++) {
                System.out.print(numerosLoterieHebdomadaire[i][j] + " ");
            }
            System.out.println();
        }
    }
    }
