import java.security.spec.RSAOtherPrimeInfo;

public class OperateursApp {

    public static void main(String[] args) {

        String typeVoiture = "Dodge Challenger SRT";
        int prix = 36000;
        int argentDansLaBanque = 1000000;
        boolean estEndommagee = true;

        System.out.println("prix: £" + prix);
        int augmentationPrix = prix + 1000;
        System.out.println("prix augmenté: £ " + augmentationPrix);
        int diminutionPrix = prix - 1000;
        System.out.println("prix diminué: £" + diminutionPrix);
        int prixDeuxVoitire = prix * 2;
        System.out.println("prix de deux voitures: £" + prixDeuxVoitire);
        int dodgeQueVousPouvezAcheter = argentDansLaBanque / prix;
        System.out.println("les dodges que vous pouvez acheter: £" + dodgeQueVousPouvezAcheter);
        int argentRestant = dodgeQueVousPouvezAcheter % prix;
        System.out.println("L'argent que j'aurai après avoir acheter: " + dodgeQueVousPouvezAcheter + "Dodge Challenger " +
                ": £" + argentRestant);

        int prixNegatif = -36000;
        int prixNegatifAvecSignePlus = +prixNegatif;
        System.out.println("prix négatif avec signe plus: £" + prixNegatifAvecSignePlus);

        int prixNegatifAvecSigneMoins = -prixNegatif;
        System.out.println("prix négatif avec signe moins: £" + prixNegatifAvecSigneMoins);
        int augmentationPrixUnuionEuro = ++prix;
        System.out.println("prix après un euro d'augmentation: £" + augmentationPrixUnuionEuro);
        System.out.println("prix original après un euro d'augmentation: £" + prix);
        int diminutionPrixUnuionEuro = --prix;
        System.out.println("prix après un euro de diminution: £" + diminutionPrixUnuionEuro);
        System.out.println("prix original après un euro de diminution: £" + prix);

        System.out.println("la voiture est endomagée: " + !estEndommagee);
        System.out.println("la voiture typeVoiture est une string " + (typeVoiture instanceof String));

        String textEndommagee = !estEndommagee? "la voiture est endommagée" : "la voiture n'est oas endommagée";
        System.out.println(textEndommagee);
        String vautLaPeineDeVoir = !estEndommagee || prix < 40000 ? "ça vaut la peine de voir" : "ca vaut pas la pzeine de voir";
        String vautLaPeineDeReparere = estEndommagee && prix < 37000 ? "ça vaut la peine de la réparer" : "ca vaut pas la peine de la réparer";
        System.out.println(vautLaPeineDeVoir );
        System.out.println(vautLaPeineDeReparere );
        System.out.println();

        System.out.println("le prix original " + prix);
        prix = prix +  1000;
        System.out.println("prix augmenté: £ " + prix);
        prix -= 1000;
        System.out.println("prix diminué: £ " + prix);
        prix *= 2;
        System.out.println("prix doublé: £ " + prix );
        prix /= 2;
        System.out.println("la moitié du prix: £ " + prix );
        prix %= 10000;
        System.out.println("le reste  du prix: £ " + prix );
    }
}
